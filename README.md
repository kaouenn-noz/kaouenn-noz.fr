# kaouenn-noz.fr
Ceci est le dépôt public du site [kaouenn-noz.fr](https://kaouenn-noz.fr), c'est-à-dire son **contenu**. Nous avons un autre dépôt, [hugo-tufte-kaouenn](https://framagit.org/kaouenn-noz/hugo-tufte-kaouenn), qui lui est dédié au **thème**.

## Kaouenn-noz
Kaouenn-noz est un ensemble d'individus qui se réunissent pour pratiquer ensemble des techniques, arpenter les sciences dans et avec le vivant, revisiter la technologie par le critique.
**Kaouenn** signifie en breton chouette, ces rapaces nocturnes qui veillent en vol silencieux !

## Modifier, ajouter du contenu
### Copier le dépôt en local, sur son ordinateur
1. installer [hugo](https://gohugo.io/getting-started/installing/), le moteur que l'on utilise pour générer les pages à partir de documents écrits en [markdown](https://fr.wikipedia.org/wiki/Markdown).
2. cloner le dépôt : `git clone --recurse-submodules https://framagit.org/kaouenn-noz/kaouenn-noz.fr` (`--recurse-submodules` permet de cloner également le [dépôt du thème](https://framagit.org/kaouenn-noz/hugo-tufte-kaouenn))
3. se rendre dans le dépôt (`cd kaouenn-noz.fr`) et lancer Hugo : `hugo serve`. Les pages sont compilées et un petit serveur **local** vous permet de voir le résultat, en général à l'adresse http://localhost:1313. À chaque modification d'un fichier, le site se met à jour.

### Contribuer
Une fois cloné le dépôt, si vous avez les permissions suffisantes, vous pourrez créer une branche et proposer une *Merge request*. N'hésitez pas à écrire une *issue* pour demander les permissions nécessaires.

Ensuite : 
1. Créer une nouvelle branche
	- Soit sur l'interface de Gitlab (page principale de projet, au-dessus de la liste des fichiers, le petit `+`)
	- Soit en local : `git branch ma-nouvelle-branche`. Il faudra ensuite pousser cette branche sur Gitlab en lançant la commande suivante : `git push -u origin ma-nouvelle-branche`.
	- Soit, si votre proposition fait l'objet d'une *issue*, vous pouvez cliquer sur *"Create merge request"*. Cela créera automatiquement une nouvelle branche.

Travaillez en local sur cette branche, *commitez* (`git add . && git commit -m "ce que j'ai fait"`) vos avancées. 
Ensuite, poussez (`git push`) votre travail. Vous devriez voir votre travail dans le dépôt distant du projet (sur Gitlab donc), à la bonne branche.

Si vous avez cliqué sur *"Create merge request"*, la *Merge request* est automatiquement mise à jour.

2. Créer une *Merge request*
Si vous n'avez pas encore de *Merge request*, vous pouvez en créer une (menu de gauche → *Merge Requests* → *New merge request*). 
Sélectionnez votre branche à vous dans *Source branch*, et sélectionnez la branche `master` dans *Target branch*. 

Quelqu'un fera une révision de code et votre modification sera publiée !


## Licences
Le thème `hugo-tufte-kaouenn` (licence MIT) est un dérivé du thème [`Tufte Hugo`](https://github.com/shawnohare/hugo-tufte) également sous licence MIT.

Tout le reste est soumis à, sauf mention contraire :
- la licence [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) pour les contenus écrits du site web
- la [Free Art License 1.3](https://artlibre.org/licence/lal/en/) pour les illustrations graphiques


## Autrices, auteurs
[@Xavcc](https://framagit.org/Xavcc), [@matthieubrient](https://framagit.org/matthieubrient), [@eorn](https://framagit.org/eorn)… et vous ?
